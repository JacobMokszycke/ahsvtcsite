package org.ahs.model;

public class Location {

	private int locationId;
	private String locationCity;
	private String locationState;
    private String locationCountry;
    
    public int getLocationId() {
        return locationId;
    }
    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }
    public String getLocationCity() {
        return locationCity;
    }
    public void setLocationCity(String locationCity) {
        this.locationCity = locationCity;
    }
    public String getLocationState() {
        return locationState;
    }
    public void setLocationState(String locationState) {
        this.locationState = locationState;
    }
    public String getLocationCountry() {
        return locationCountry;
    }
    public void setLocationCountry(String locationCountry) {
        this.locationCountry = locationCountry;
    }
}