package org.ahs.model;

public class Agent {

	private int agentId;
	private String agentFirst_name;
	private String agentLast_name;
	private String agentPhone_number;
	private String agentEmail;
	private String agentBio;
	private String agentImage;
	private int agentLocationid;
    
    public int getAgentId() {
        return agentId;
    }
    public void setAgentId(int agentId) {
        this.agentId = agentId;
    }
    public String getAgentFirst_name() {
        return agentFirst_name;
    }
    public void setAgentFirst_name(String agentFirst_name) {
        this.agentFirst_name = agentFirst_name;
    }
    public String getAgentLast_name() {
        return agentLast_name;
    }
    public void setAgentLast_name(String agentLast_name) {
        this.agentLast_name = agentLast_name;
    }
    public String getAgentPhone_number() {
        return agentPhone_number;
    }
   public void setAgentPhone_number(String agentPhone_number) {
        this.agentPhone_number = agentPhone_number;
    }
   public String getAgentEmail() {
       return agentEmail;
   }
   public void setAgentEmail(String agentEmail) {
       this.agentEmail = agentEmail;
   }
   public String getAgentBio() {
       return agentBio;
   }
   public void setAgentBio(String agentBio) {
       this.agentBio = agentBio;
   }
   public String getAgentImage() {
       return agentImage;
   }
   public void setAgentImage(String agentImage) {
       this.agentImage = agentImage;
   }
   public int getAgentLocationid() {
       return agentLocationid;
   }
   public void setAgentLocationid(int agentLocationid) {
       this.agentLocationid = agentLocationid;
   }

    
}
