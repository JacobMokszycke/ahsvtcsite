package org.ahs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.ahs.model.Location;
import org.ahs.util.DbUtil;

public class LocationDao {

    private Connection connection;
 
    public LocationDao() {
        connection = DbUtil.getConnection();
    }

  
    public List<Location> getAllLocations() {
        List<Location> locations = new ArrayList<Location>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from location");
            while (rs.next()) {
                Location location = new Location();
                location.setLocationId(rs.getInt("id"));
                location.setLocationCity(rs.getString("city"));
                location.setLocationState(rs.getString("state"));
                location.setLocationCountry(rs.getString("country"));
                locations.add(location);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return locations;
    }

    public Location getLocationById(int locationId) {
        Location location = new Location();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("select * from location where id=?");
            preparedStatement.setInt(1, locationId);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                location.setLocationId(rs.getInt("id"));
                location.setLocationCity(rs.getString("city"));
                location.setLocationCountry(rs.getString("state"));
                location.setLocationState(rs.getString("country"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return location;
    }
}