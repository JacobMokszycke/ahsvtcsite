
package org.ahs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.ahs.model.Agent;
import org.ahs.util.DbUtil;

public class AgentDao {

    private Connection connection;
 
    public AgentDao() {
        connection = DbUtil.getConnection();
    }

      

    public List<Agent> getAllAgents(int locationId) {
        List<Agent> agents = new ArrayList<Agent>();
        try {
        	PreparedStatement preparedStatement = connection.
                    prepareStatement("select * from agents where id=?");
            preparedStatement.setInt(1, locationId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Agent agent = new Agent();
                agent.setAgentId(rs.getInt("id"));
                agent.setAgentFirst_name(rs.getString("first_name"));
                agent.setAgentLast_name(rs.getString("last_name"));
                agent.setAgentPhone_number(rs.getString("phone_number"));
                agent.setAgentEmail(rs.getString("email"));
                agent.setAgentBio(rs.getString("bio"));
                agent.setAgentImage(rs.getString("image"));
                agent.setAgentLocationid(rs.getInt("locationid"));
                agents.add(agent);
            }
            System.out.println("get all agents"+preparedStatement.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return agents;
    }

    public Agent getAgentById(int agentId) {
        Agent agent = new Agent();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("select * from agents where id=?");
            preparedStatement.setInt(1, agentId);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                agent.setAgentId(rs.getInt("id"));
                agent.setAgentFirst_name(rs.getString("first_name"));
                agent.setAgentLast_name(rs.getString("last_name"));
                agent.setAgentPhone_number(rs.getString("phone_number"));
                agent.setAgentEmail(rs.getString("email"));
                agent.setAgentBio(rs.getString("bio"));
                agent.setAgentImage(rs.getString("image"));
                agent.setAgentLocationid(rs.getInt("locationid"));
                }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return agent;
    }
}