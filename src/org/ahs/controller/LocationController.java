package org.ahs.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ahs.dao.LocationDao;
import org.ahs.model.Location;



public class LocationController extends HttpServlet 
{
    private static final long serialVersionUID = 1L;
    private static String LIST_LOCATION = "/listLocation.jsp";
    private LocationDao locationDao;
    public LocationController() {
        super();
        locationDao = new LocationDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward="";
        String action = request.getParameter("action");
        
        System.out.println("action=" + action);
        
            forward = LIST_LOCATION;
            System.out.println("test3");
            request.setAttribute("locations", locationDao.getAllLocations());
        

        System.out.println("test2");
        
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Location location = new Location();
        System.out.println("test5 post");
        location.setLocationCity(request.getParameter("locationCity"));
        location.setLocationState(request.getParameter("locationState"));
        location.setLocationCountry(request.getParameter("locationCountry"));
        String id = request.getParameter("locationId");
        

        RequestDispatcher view = request.getRequestDispatcher(LIST_LOCATION);
        request.setAttribute("locations", locationDao.getAllLocations());
        view.forward(request, response);
    }
}