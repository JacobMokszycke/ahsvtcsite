package org.ahs.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ahs.dao.LocationDao;
import org.ahs.model.Location;
import org.ahs.dao.AgentDao;
import org.ahs.model.Agent;;


public class AgentController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static String LIST_AGENT = "/listAgent.jsp";
    private LocationDao locationDao;
    private AgentDao agentDao;
    public AgentController() {
        super();
        locationDao = new LocationDao();
        agentDao= new AgentDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward="";
        
        String action = request.getParameter("action");
        System.out.println("action=" + action);
        forward = LIST_AGENT;
            System.out.println("test1");
            int locationId = Integer.parseInt(request.getParameter("locationId"));
            request.setAttribute("agents", agentDao.getAllAgents(locationId));
            
            
   
        System.out.println("locationId is: " + locationId);
        
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Agent agent = new Agent();
        System.out.println("test5 post");
        agent.setAgentFirst_name(request.getParameter("agentFirst_name"));
        agent.setAgentLast_name(request.getParameter("agentLast_name"));
        agent.setAgentPhone_number(request.getParameter("agentPhone_number"));
        agent.setAgentEmail(request.getParameter("agentEmail"));
        agent.setAgentBio(request.getParameter("agentBio"));
        agent.setAgentImage(request.getParameter("agentImage"));
        String agentTemp=request.getParameter("agentLocationid");
        agent.setAgentLocationid(Integer.parseInt(agentTemp));
        String id = request.getParameter("agentId");
        
        if(id == null || id.isEmpty())
        {
//        	agentDao.addAgent(agent);
        }
        else
        {
            agent.setAgentId(Integer.parseInt(id));
 //           agentDao.updateAgent(agent);
        }
 //       RequestDispatcher view = request.getRequestDispatcher(LIST_LOCATION);
 //       request.setAttribute("agents", agentDao.getAllAgents(locationId));
 //       view.forward(request, response);
    }
}