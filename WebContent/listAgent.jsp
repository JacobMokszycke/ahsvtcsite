 
 <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!-- 

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

 -->
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Show All Agents</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div id="wrapper">
<header>
	<h1>Agents</h1>
</header>
<aside>
<br>
</aside>
<article>
    <table id="bluetable">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Biography</th>
                <th>Image</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${agents}" var="agent">
                <tr>
                    <td><c:out value="${agent.agentFirst_name}" /></td>
                    <td><c:out value="${agent.agentLast_name}" /></td>
                    <td><c:out value="${agent.agentPhone_number}" /></td>
                    <td><c:out value="${agent.agentEmail}" /></td>
                    <td><c:out value="${fn:substring(agent.agentBio, 0, 20)}" />...</td>
                    <!--     <c:out value="${fn:substring(agent.agentBio, 0, 20)}" />...     -->
                    <td><img src=<c:out value="${agent.agentImage}" /> alt="<c:out value="${agent.agentFirst_name}" />" style="width:42px;height:42px;border:0;"></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
<!--    <a href="default.asp">
  <img src=<c:out value="thumbnails/tree.jpg"/> alt="HTML tutorial" style="width:42px;height:42px;border:0;">
</a>
-->
</article>
<aside>
<br>
</aside>
</div>
</body>
</html>
