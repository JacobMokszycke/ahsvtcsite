<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!--
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
-->

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Show All Locations</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="wrapper">
    <header>
    	<h1>Locations</h1>
    </header>
    <aside>
    <br>
    </aside>
    <article>
    <table id="bluetable">
        <thead>
            <tr>
                <th>ID</th>
                <th>Place</th>
                <th>State</th>
                <th>Country</th>
                <th colspan=3>Agents</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${locations}" var="location">
                <tr>
                    <td><c:out value="${location.locationId}" /></td>
                    <td><c:out value="${location.locationCity}" /></td>
                    <td><c:out value="${location.locationState}" /></td>
                    <td><c:out value="${location.locationCountry}" /></td>
          	     	<td><a href="AgentController?locationId=<c:out value="${location.locationId}"/>">Agents at this location</a></td>
                 </tr>
            </c:forEach>
        </tbody>
    </table>
    </article>
    <aside>
    <br>
    </aside>
    </div>
</body>
</html>